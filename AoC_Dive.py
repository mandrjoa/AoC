# Function to calculate the result
def calculate_result(commands):
    horizontal_position = 0
    depth = 0

    for command in commands:
        action, value = command.split()
        value = int(value)

        if action == "forward":
            horizontal_position += value
        elif action == "down":
            depth -= value
        elif action == "up":
            depth += value

    return abs(horizontal_position) * abs(depth)

# Read commands from numbers.txt and format into a list
file_path = "numbers.txt"  # Update the path

commands = []

with open(file_path, 'r') as file:
    for line in file:
        commands.append(line.strip())

# Calculate the result using the calculate_result function
result = calculate_result(commands)

# Print the result
print(f"Result (Horizontal * Depth): {result}")
