# Original function to count increases
def count_increases_in_measurements(measurements):
    count_increases = 0

    for i in range(2, len(measurements)):
        window_sum = measurements[i - 2] + measurements[i - 1] + measurements[i]
        prev_window_sum = measurements[i - 3] + measurements[i - 2] + measurements[i - 1]

        if window_sum > prev_window_sum:
            count_increases += 1

    return count_increases

# Unit tests
import unittest

class TestCountIncreases(unittest.TestCase):
    def test_count_increases(self):
        measurements1 = [1, 2, 3, 2, 4, 5, 6, 1, 1, 2]
        measurements2 = [5, 2, 8, 1, 9, 3]
        measurements3 = [1, 1, 1, 1, 1, 1]

        self.assertEqual(count_increases_in_measurements(measurements1), 5)
        self.assertEqual(count_increases_in_measurements(measurements2), 2)  
        self.assertEqual(count_increases_in_measurements(measurements3), 0)

if __name__ == '__main__':
    unittest.main()
