#Feature: To check if a password is valid based on the policy and count the amount of valid passwords.
def is_valid_password(policy, password):
    min_count, max_count, letter = policy
    count = password.count(letter)
    return min_count <= count <= max_count

# Function to parse the input line and return policy and pwd
def parse_line(line):
    parts = line.split()
    min_max = parts[0].split('-')
    min_count = int(min_max[0])
    max_count = int(min_max[1])
    letter = parts[1][0]
    password = parts[2]
    return (min_count, max_count, letter), password

# Read the input file and count how many valid passwords
valid_count = 0
with open('input.txt', 'r') as file:
    for line in file:
        policy, password = parse_line(line)
        if is_valid_password(policy, password):
            valid_count += 1

print("Number of valid passwords:", valid_count)