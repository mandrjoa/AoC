import unittest
from AoC_SonarSweeps import count_increasing_measurements

class TestCountIncreasingMeasurements(unittest.TestCase):
    def test_count_increasing_measurements_example(self):
        depths = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263]
        self.assertEqual(count_increasing_measurements(depths), 7)

    #test an empty list
    def test_count_increasing_measurements_empty_list(self):
        depths = []
        self.assertEqual(count_increasing_measurements(depths), 0)
    #test list with a single value
    def test_count_increasing_measurements_single_measurement(self):
        depths = [199]
        self.assertEqual(count_increasing_measurements(depths), 0)
    #test list where all depths are increasing
    def test_count_increasing_measurements_all_increasing(self):
        depths = [100, 200, 300, 400]
        self.assertEqual(count_increasing_measurements(depths), 3)
    #test list where all depths are decreasing
    def test_count_increasing_measurements_all_decreasing(self):
        depths = [400, 300, 200, 100]
        self.assertEqual(count_increasing_measurements(depths), 0)

if __name__ == '__main__':
    unittest.main()
