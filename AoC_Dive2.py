# Function to calculate the result
def calculate_result(commands):
    horizontal_position = 0
    depth = 0
    aim = 0  # New variable to track aim

    for command in commands:
        action, value = command.split()
        value = int(value)

        if action == "forward":
            horizontal_position += value
            depth += aim * value  # Increase depth by aim multiplied by value
        elif action == "down":
            aim += value  # Increase aim when going down
        elif action == "up":
            aim -= value  # Decrease aim when going up

    return abs(horizontal_position) * abs(depth)

# Read commands from numbers.txt and format into a list
file_path = "numbers.txt"  # Update the path

commands = []

with open(file_path, 'r') as file:
    for line in file:
        commands.append(line.strip())

# Calculate the result using the calculate_result function
aim = calculate_result(commands)

# Print the aim
print(f"Aim (Horizontal * Depth): {aim}")
