# Feature: Check if pwd valid based on new policy
def is_valid_password(policy, password):
    pos1, pos2, letter = policy
    return (password[pos1 - 1] == letter) ^ (password[pos2 - 1] == letter)

# Function to parse the input line and return the policy and password
def parse_line(line):
    parts = line.split()
    pos1, pos2 = map(int, parts[0].split('-'))
    letter = parts[1][0]
    password = parts[2]
    return (pos1, pos2, letter), password

# Read the input file and count valid passwords
valid_count = 0
with open('input.txt', 'r') as file:
    for line in file:
        policy, password = parse_line(line)
        if is_valid_password(policy, password):
            valid_count += 1

print("Number of valid passwords:", valid_count)