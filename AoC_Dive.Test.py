import unittest

# Function to calculate the result
def calculate_result(commands):
    horizontal_position = 0
    depth = 0

    for command in commands:
        action, value = command.split()
        value = int(value)

        if action == "forward":
            horizontal_position += value
        elif action == "down":
            depth -= value
        elif action == "up":
            depth += value

    return abs(horizontal_position) * abs(depth)



class TestCalculateResult(unittest.TestCase):

    def test_result_calculation(self):
        # Test the calculate_result function with various input commands
        self.assertEqual(calculate_result(["forward 1", "forward 2", "down 5"]), 15)
        self.assertEqual(calculate_result(["down 5", "up 2", "forward 3"]), 15)
        self.assertEqual(calculate_result(["forward 4", "down 2", "up 1"]), 4)

    def test_empty_input(self):
        # Test with an empty list of commands
        self.assertEqual(calculate_result([]), 0)

    def test_single_command(self):
        # Test with a single command
        self.assertEqual(calculate_result(["forward 5"]), 5)
        self.assertEqual(calculate_result(["down 3"]), 3)
        self.assertEqual(calculate_result(["up 2"]), 2)

if __name__ == '__main__':
    unittest.main()
