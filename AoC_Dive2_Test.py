import unittest
from AoC_Dive2 import calculate_result  

class TestCalculateAim(unittest.TestCase):

    def test_initial_state(self):
        # Test that the initial aim is 0
        self.assertEqual(calculate_result([]), 0)

    def test_forward_command(self):
        # Test the forward command
        commands = ["forward 5"]
        # Horizontal position increases by 5, Depth = 0 + (0 * 5) = 0
        self.assertEqual(calculate_result(commands), 0)

    def test_down_command(self):
        # Test the down command
        commands = ["down 5"]
        # Horizontal position = 0, Depth increases by 5, Aim = 5
        self.assertEqual(calculate_result(commands), 5)

    def test_up_command(self):
        # Test the up command
        commands = ["up 5"]
        # Horizontal position = 0, Depth decreases by 5, Aim = -5
        self.assertEqual(calculate_result(commands), -5)

    def test_combined_commands(self):
        # Test a combination of commands
        commands = ["down 5", "forward 3", "up 2", "forward 4"]
        # Horizontal position = 3 + 4 = 7, Depth = 5 + (5 * 3) - (2 * 4) = 17, Aim = 5
        self.assertEqual(calculate_result(commands), 7 * 17)

if __name__ == '__main__':
    unittest.main()
